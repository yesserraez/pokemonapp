//
//  ViewController.swift
//  pokemonApp
//
//  Created by Yessenia Erraez on 13/6/18.
//  Copyright © 2018 Yessenia Erraez. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
 
    var pokemon:[Pokemon] = []
    
    @IBOutlet weak var pokemonTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       let network = Network()
        network.getAllPokemon { (pokemonArray) in
            self.pokemon =  pokemonArray
            self.pokemonTableView.reloadData()
            
        }
    }

 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
       cell.pokemon = pokemon[indexPath.row]
        cell.fillData()
        return cell
    }
    

}

