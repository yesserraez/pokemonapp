//
//  PokemonTableViewCell.swift
//  pokemonApp
//
//  Created by Yessenia Erraez on 20/6/18.
//  Copyright © 2018 Yessenia Erraez. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nombreTextField: UITextField!
    
    @IBOutlet weak var pesoTextField: UITextField!
    
    @IBOutlet weak var alturaTextField: UITextField!
    
    @IBOutlet weak var pokemonImage: UIImageView!
    var pokemon:SPokemon!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func fillData(){
        nombreTextField.text = "\(pokemon.nombre!)"
        pesoTextField.text = "\(pokemon.peso!)"
        alturaTextField.text = "\(pokemon.altura!)"
        if pokemon.imagen == nil {
            
            let bm = Network()
            
            bm.getImage((pokemon?.id)!, completionHandler: { (imageR) in
                DispatchQueue.main.async {
                    self.pokemonImage.image = imageR
                }
                
            })
            
        } else {
            
            pokemonImage.image = pokemon.imagen
            
        }
        
        
    }

}
