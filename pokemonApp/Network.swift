//
//  Network.swift
//  pokemonApp
//
//  Created by Yessenia Erraez on 13/6/18.
//  Copyright © 2018 Yessenia Erraez. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper
class Network {
    func getAllPokemon(completion:@escaping ([Pokemon])->()) {
        var pokemonArray:[Pokemon] = []
        let group = DispatchGroup() //ayudan a manejar grupos de cosas asincronas(promesas)
        for i in 1...8{
            group.enter()
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseJSON { response in
                guard let data = response.data else{
                    
                    print("Error")
                    return
                }
                guard let pokemon = try? JSONDecoder().decode(Pokemon.self, from: data) else {
                    print("Error decoding Pokemon")
                    return
                }
                //  print(pokemon)
                pokemonArray.append(pokemon)
                group.leave()
            }
        }
        group.notify(queue: .main){
            //espera a que todo lo q entro haya salido
            completion(pokemonArray)
            
        }
    }
    
    func getPokemon(_ url:String){
        Alamofire.request(url).responseObject { (response: DataResponse<SPokemon>) in
            
            let spokemon = response.result.value
            
            pokemonArray += [spokemon!]
            contador = contador! - 1
    }
}
    func getImage(_ id:Int, completionHandler: @escaping(UIImage)->()){
        
        let url = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png"
        
        Alamofire.request(url).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
        
    }
}
